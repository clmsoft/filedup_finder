#!/usr/bin/env python
# ---------------------------------------------------------------------------
# filedup_finder.py
# ---------------------------------------------------------------------------
"""
Using the passed filename and directory arguments, find files that have the
same checksums. Each filename is added to the list for comparison and each
directory and its subdirectories are processed for all the files they contain.
"""

from __future__ import print_function  # so print works like python3 everywhere

import os
import sys
from stat import ST_SIZE, S_ISDIR, ST_MODE, S_ISREG, S_ISLNK, ST_CTIME, ST_MTIME, ST_ATIME
from datetime import datetime
import gzip  # for extracting gzipped files
import hashlib  # for hashing functions

# ---------------------------------------------------------------------------
# global variables
# ---------------------------------------------------------------------------

MAX_ITEMS = -1  # limit the number of files we will work with at one time

LOG_DEBUG = False
LOG_INFO = False
EXTRACT_GZIPS = False

item_count = 0

files_and_sums = {}


# ---------------------------------------------------------------------------
# functions
# ---------------------------------------------------------------------------

def print_stderr(text):
    print("{}".format(text), file=sys.stderr)


def log_debug(text):
    if LOG_DEBUG:
        print_stderr("DEBUG: {}".format(text))


def log_info(text):
    if LOG_INFO:
        print_stderr("INFO : {}".format(text))


def log_error(text):
    print_stderr("\nERROR: {}\n".format(text))


def report_problem(error):
    """
    Callback called when there is a problem walking the directory tree
    """
    log_error("problem with file {}".format(error.filename))


def show_directory_info(dirpath, process_file):
    global item_count
    for root, dirs, files in os.walk(dirpath, onerror=report_problem):
        process_file(root)
        if '.DS_Store' in files:
            files.remove('.DS_Store')  # ignore Apple Finder dispay files
        for name in files:
            process_file(os.path.join(root, name))
            if '.svn' in dirs:
                dirs.remove('.svn')  # don't visit svn directories
            if '.git' in dirs:
                dirs.remove('.git')  # don't visit git directories
            if (MAX_ITEMS > -1) and (item_count >= MAX_ITEMS):
                break
        if (MAX_ITEMS > -1) and (item_count >= MAX_ITEMS):
            break


def format_datetime(timestamp):
    # FORMAT = datetime.isoformat(' ')	# "%Y-%d-%m %H:%M:%S"
    # return dt.strftime(FORMAT)
    dt = datetime.fromtimestamp(timestamp)
    return dt.isoformat(' ')


def calc_checksum(filepath):
    global EXTRACT_GZIPS
    data = ""
    if EXTRACT_GZIPS and (filepath.endswith('.gz')):
        with gzip.GzipFile(filepath, "rb") as file_data:
            data = file_data.read()
    else:
        with open(filepath, "rb") as file_data:
            data = file_data.read()
    checksum = hashlib.md5(data).hexdigest()
    return checksum


def get_file_info(filepath):
    """
    Get info about this file and return a Dict containing the info.

    This works only for files, so do not pass other things to this method!
    """
    file_info = {}
    mode = os.lstat(filepath)
    file_info['name'] = filepath
    file_info['size'] = mode[ST_SIZE]
    file_info['created_time'] = format_datetime(mode[ST_CTIME])
    file_info['modified_time'] = format_datetime(mode[ST_MTIME])
    file_info['last_accessed_time'] = format_datetime(mode[ST_ATIME])
    file_info['checksum'] = calc_checksum(filepath)
    return file_info


def show_file_info(filepath):
    global item_count
    mode = os.lstat(filepath)
    if S_ISDIR(mode[ST_MODE]):
        log_info('{}  "{}"'.format("=" * 32, filepath))
    elif S_ISREG(mode[ST_MODE]):
        file_info = get_file_info(filepath)
        item = []
        if file_info['checksum'] in files_and_sums:
            item = files_and_sums[file_info['checksum']]
        item.append(file_info)
        files_and_sums[file_info['checksum']] = item
        if LOG_DEBUG:
            log_debug("Checksum: {}  Name: '{}'  Size: {}  Created: {}  Last Modified: {}  Last Accessed: {}".format(
                file_info['checksum'],
                file_info['name'],
                file_info['size'],
                file_info['created_time'],
                file_info['modified_time'],
                file_info['last_accessed_time']
            ))
        else:
            log_info('{}  "{}"'.format(
                file_info['checksum'],
                file_info['name']
            ))
        item_count += 1  # increment the number of files we have processed
    elif S_ISLNK(mode[ST_MODE]):
        broken_link = not os.path.exists(os.path.realpath(filepath))
        link_str = ""
        if broken_link:
            link_str += "broken "
        link_str += "link"
        text = '{0} {1} -->  "{2}" is a {1} to "{3}"'
        log_info(text.format(" " * (27 - len(link_str)), link_str, filepath, os.path.realpath(filepath)))


# ---------------------------------------------------------------------------
# main program
# ---------------------------------------------------------------------------

if __name__ == '__main__':
    import argparse

    #
    # handle command line
    #
    parser = argparse.ArgumentParser(add_help=True, description=__doc__)
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='show progress to stderr as files are being processed')
    parser.add_argument('-d', '--debug', action='store_true', default=False,
                        help='show more detailed (DEBUG) information to stderr as files are being processed')
    parser.add_argument('-x', '--gzextract', action='store_true', default=False,
                        help='extract files ending in .gz and compare the contents instead of the gzip file. ' +
                             'This only works if the gzips contain only one file each.'
                        )
    parser.add_argument('-l', '--limit', type=int, default=-1,
                        help='process at most this many files')
    parser.add_argument('files', nargs="+")
    args = parser.parse_args()

    LOG_INFO = args.verbose
    LOG_DEBUG = args.debug
    MAX_ITEMS = args.limit
    EXTRACT_GZIPS = args.gzextract

    for pathname in args.files:
        try:
            mode = os.stat(pathname).st_mode
            if S_ISDIR(mode):
                # they passed a directory so process all the files within it
                show_directory_info(pathname, show_file_info)
            elif S_ISREG(mode):
                # they passed a filename so deal with the info directly
                show_file_info(pathname)
                if (MAX_ITEMS > -1) and (item_count >= MAX_ITEMS):
                    break
        except OSError:
            log_error("I don't know what to do with '{}'".format(pathname))

    print("\nProcessed {} files\n".format(item_count))
    #
    # find all the checksums that apply to more than one file
    #
    for k in sorted(files_and_sums):
        item = files_and_sums[k]
        if len(item) > 1:
            print("These {} files have the checksum {}".format(len(item), k))
            for i in item:
                print('\tSize: {:>13,d}  Created: {}  Last modified: {}  Name: "{}"'.format(
                    i['size'],
                    i['created_time'],
                    i['modified_time'],
                    i['name']))

# ---------------------------------------------------------------------------
# end filedup_finder.py
